# caensyproxy conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/caensyproxy"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS caensyproxy module
